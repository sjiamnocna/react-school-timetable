# React school timetable component
- Classic school timetable look
    - Days as rows
    - Subjects and hours as columns
- Handles overlapping classes
- Allows modifications using React props passed with lessons list

## Start
Created using Create-react-app

## Import component
`import Timetable from 'react-school-timetable'`

Use as
`<Timetable rows={...}>`
import Row from './components/row/row.comp'

import './App.scss'
import { classTimeRange } from './utils/functions';

const Timetable = ({rows, colWidth}) => {
  colWidth = colWidth ? colWidth : 140;
  const limits = classTimeRange(rows)
  return(
    <div className='timetable-container'>
      {
        rows.map( (day, i) => <Row key={i} className={`row${i % 2 ? 'odd' : 'even'}${day.className ? ' ' + day.className : ''}`} colWidth={colWidth} beginTime={limits[0]} {...day} /> )
      }
    </div>
  )
}

export default Timetable
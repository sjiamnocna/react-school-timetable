import React from 'react'
import { timeDifference } from '../../utils/functions'
import Lesson from '../lesson/lesson.comp'


const ClassList = ({ colWidth, subjects, beginTime }) => {
    return (
        <div className='content'>
            {
                subjects.map(((item, i) => {
                    const posLeft = timeDifference(beginTime, item.time.begin) * colWidth / 60,
                        width = Math.abs(timeDifference(item.time.end, item.time.begin) * colWidth / 60)

                    return (
                        // margin left relative to the previous class end
                        <Lesson
                            style={{
                                // position left
                                'left': posLeft,
                                'width': width,
                            }}
                            key={i}
                            {...item}
                        />
                    )
                }))
            }
        </div>
    )
}

export default ClassList
import React from 'react'
import { timeReadableFormat } from '../../utils/functions'

import './lesson.scss'

/**
 * Lesson item
 * @param object props
 * @returns 
 */
const Lesson = ({ shortcut, title, room, teacher, time, className, ...props }) => {
    return (
        <div
            className={`lesson-item${className ? ' ' + className : ''}`} // allows control the look of the item
            {...props}
        >
            <div className="lesson-container">
                <time>
                    <span className="from">{timeReadableFormat(time.begin)}</span>
                    <span className="to">{timeReadableFormat(time.end)}</span>
                </time>
                <div className='lesson-data'>
                    <abbr title={title}>{shortcut}</abbr>
                    <p className="row teacher">{teacher}</p>
                    <p className="room">{room}</p>
                </div>
            </div>
        </div>
    )
}

export default Lesson
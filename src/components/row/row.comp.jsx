import React from 'react'
import { overlapRows } from '../../utils/functions';
import ClassList from '../classList/classlist.comp';

import './row.scss'

const Row = ({ colWidth, className, subjects, title, note, beginTime, ...props }) => {
    const lists = []

    console.log(subjects)
    let overlap = overlapRows(subjects)
    console.log('overlap', overlap, subjects)

    if (!overlap) {
        console.log('no overlap'. subjects);
    }
    
    while (overlap){
        // push cleared list
        lists.push(overlap[0])

        let cond = overlapRows(overlap[1])
        if (!Array.isArray(cond)){
            // finally push the rest and go to render
            lists.push(overlap[1])
            break
        }

        overlap = cond;
    }

    return (
        <div className={`row${className ? ' ' + className : ''}`}>
            <div className='title'>
                <p className='desc'>{title}</p>
                {
                    note ? <p className='note'>{note}</p> : null
                }
            </div>
            <div className='rows'>
            {
                lists.map((item, i) => {
                    console.log('classlists', item);
                    return <ClassList colWidth={colWidth} key={i} subjects={item} beginTime={beginTime} />
                })
            }            
            </div>
        </div>
    )
}

export default Row
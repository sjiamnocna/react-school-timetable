import React from 'react';
import ReactDOM from 'react-dom';
import Timetable from './App';
import reportWebVitals from './reportWebVitals';
import { generateHeaderClasses } from './utils/functions';

ReactDOM.render(
  <React.StrictMode>
    <Timetable
      rows={[
        {
          title: '46.',
          note: 'sudý',
          className: 'timetable-head',
          subjects: null,
        },
        {
          title: 'Po',
          note: '14. 5.',
          className: 'timetable-day',
          subjects: [{
            title: 'Ergonomie a bezpečnost práce',
            shortcut: 'KTE/EBP@',
            teacher: 'Nováková',
            room: 'ZI-P4',
            time: {
              begin: 845,
              end: 930
            },
            className: 'orange',
          },
          {
            title: 'Srovnávací pedagogika',
            shortcut: 'KPG/PES@',
            teacher: 'Novák',
            room: 'ZI-N3',
            time: {
              begin: 945,
              end: 1029
            },
            className: 'red grayscale',
          },],
        },
        {
          title: 'Út',
          note: '15. 5.',
          className: 'timetable-day',
          subjects: [{
            title: 'Ergonomie a bezpečnost práce',
            shortcut: 'KTE/EBP@',
            teacher: 'Nováková',
            room: 'ZI-P4',
            time: {
              begin: 845,
              end: 930
            },
            className: 'orange',
          },
          {
            title: 'Srovnávací pedagogika',
            shortcut: 'KPG/PES@',
            teacher: 'Novák',
            room: 'ZI-N3',
            time: {
              begin: 945,
              end: 1029
            },
            className: 'red grayscale',
          },
          {
            title: 'Long one',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 845,
              end: 930
            },
            className: 'orange grayscale',
          },
          {
            title: 'Subject XYZ',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 950,
              end: 1035
            },
            className: 'red',
          },
          {
            title: 'Subject XYZ',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 950,
              end: 1035
            },
            className: 'green',
          },
          {
            title: 'Long one',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 845,
              end: 930
            },
            className: 'orange grayscale',
          },
          {
            title: 'Subject XYZ',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 950,
              end: 1035
            },
            className: 'red',
          },
          {
            title: 'Subject XYZ',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 950,
              end: 1035
            },
            className: 'green',
          },
          {
            title: 'Subject XYZ',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 1100,
              end: 1145
            },
            className: 'yellow',
          }],
        },
        {
          title: 'Út',
          note: '15. 5.',
          className: 'timetable-day',
          subjects: [{
            title: 'Ergonomie a bezpečnost práce',
            shortcut: 'KTE/EBP@',
            teacher: 'Nováková',
            room: 'ZI-P4',
            time: {
              begin: 845,
              end: 930
            },
            className: 'orange',
          },
          {
            title: 'Srovnávací pedagogika',
            shortcut: 'KPG/PES@',
            teacher: 'Novák',
            room: 'ZI-N3',
            time: {
              begin: 945,
              end: 1029
            },
            className: 'red grayscale',
          },
          {
            title: 'Long one',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 845,
              end: 930
            },
            className: 'orange grayscale',
          },
          {
            title: 'Subject XYZ',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 950,
              end: 1035
            },
            className: 'red',
          },
          {
            title: 'Subject XYZ',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 950,
              end: 1035
            },
            className: 'green',
          },
          {
            title: 'Long one',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 845,
              end: 930
            },
            className: 'orange grayscale',
          },
          {
            title: 'Subject XYZ',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 950,
              end: 1035
            },
            className: 'red',
          },
          {
            title: 'Subject XYZ',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 950,
              end: 1035
            },
            className: 'green',
          },
          {
            title: 'Subject XYZ',
            shortcut: 'XYZ',
            teacher: 'Jan Kašpárek',
            room: 'A42',
            time: {
              begin: 1100,
              end: 1145
            },
            className: 'yellow',
          }],
        },
      ]} />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

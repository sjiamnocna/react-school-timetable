/**
 * Get min and max beginning and ending times from classes
 * 
 * @param {Array} classes Array of classes in format:
 * {
    title: 'Ergonomie a bezpečnost práce',
    shortcut: 'KTE/EBP@',
    teacher: 'Nováková',
    room: 'ZI-P4',
    time: {
        begin: 845,
        end: 930
    },
    className: 'orange',
    }
 */
export const classTimeRange = (days) => {
    if (typeof days !== 'object') {
        console.error('Argument is not an array')
        return null
    }

    let limits = [Infinity, -Infinity]
    days.forEach((day) => {
        if (!day.subjects){
            return;
        }

        day.subjects.forEach((subject) => {
            if (subject.time.begin < limits[0]) {
                limits[0] = subject.time.begin
            }
            if (subject.time.end > limits[1]) {
                limits[1] = subject.time.end
            }
        })
    })

    return limits
}

export const timeDifference = (begin, end) => {
    // if end is later than beginning, revert sign
    const sign = end > begin ? 1 : -1
    if (sign < 0) {
        // switch values and continue
        const tmp = end
        end = begin
        begin = tmp
    }
    // begin/end hours + minutes
    const ahr = Number(String(begin).slice(0, -2)),
        amin = Number(String(begin).slice(-2)),
        bhr = Number(String(end).slice(0, -2)),
        bmin = Number(String(end).slice(-2))

    if (bhr === ahr) {
        // diff in minutes
        return bmin - amin
    }

    // rest of minutes to end of hour plus difference of hours times 60min
    return ((60 - amin) + (bhr - (ahr + 1)) * 60 + bmin) * sign
}

export const timeReadableFormat = (time) => {
    time = String(time)
    // last two characters
    let m = time.slice(-2)
    // characters until the one before the last
    let h = time.slice(0, -2)

    return h + ':' + m
}

export const overlapRows = (rows) => {
    if (!rows){
        return null;
    }
    const left = []
    rows.sort((first, second) => first.time.begin > second.time.begin)
    const res = rows.filter((item, i, array) => {
        if (i !== 0) {
            // cover [-1] error
            let prev = i - 1
            // go back in items and skip items marked as overlapd
            for (; prev > 0 && array[prev] && array[prev].overlap === true; prev--);

            // if end of the item is greater than current item beginning, it's 
            if (array[prev].time.end > item.time.begin) {
                array[i].overlap = true
                return true
            }
        }

        left.push(item)
        return false
    })

    return res.length > 0 ? [left, res] : null
}

export const generateHeaderClasses = (count, timeStart, timeClassLen, timeBreakLen) => {
    timeStart = timeStart ? timeStart : 700
    timeClassLen = timeClassLen && timeClassLen < 60 ? timeClassLen : 45
    timeBreakLen = timeBreakLen && timeBreakLen < 60 ? timeBreakLen : 15
    count = count ? count : 8

    let res = []
    let begin = timeStart
    let end = begin + timeClassLen

    for (let i = 0; i < count; i++) {
        res.push({
            title: i,
            time: {
                begin: begin,
                end: end
            },
            className: 'orange',
        })

        // take previous end and add break time
        begin+= end + timeBreakLen
        // rewrite 
        end = begin+timeClassLen
    }
    
    return res
}